"""
File to store progress into a file, so you can pick it up later.

Very convenient for use in batch processes.
"""
import os
import json


def store_progress(index_name, movie_number, finished, filename):
    """
    Store the content to a file.

    :param index_name: latest index that has been parsed
    :param movie_number: the latest movie number that has been indexed
    :param finished: boolean to see if all files have been finished
    :param filename: the target file for the progress.
    """
    content = {
        'last_parsed_index': index_name,
        'last_parsed_movie': movie_number,
        'finished': finished
    }
    target_file = open(filename, 'w')
    target_file.write(json.dumps(content))
    target_file.close()


def read_progress(filename):
    """
    Read the progress from the file.

    Returns None if the latest has been finished, or the dictionary.
    """
    if os.path.isfile(filename):
        target_file = open(filename, 'r')
        content = json.loads(target_file.readlines())
        target_file.close()
        if content.get('finished'):
            return None
        return content
    return None
