"""
Convert 'list'-files into an indexable file format.

- List all files that have been read
- Read each file, and create an index {
    movie_title: {
        'file_basename': value
    }
}
- Store the file to disk.

"""
from __future__ import division
import os
import csv
import json
import re

from progress_util import store_progress, read_progress


def store_content(content, target, *args, **kwargs):
    """
    Store the content in the designated file.

    :param content: the json to be stored
    :param target: target_file
    """
    target_file = open(target, 'w')
    target_file.write(json.dumps(content))
    target_file.close()


list_path = 'ftp.fu-berlin.de/pub/misc/movies/database/'
progress_file = 'indexing_progress.json'
results_file = 'movie_index.json'
movie_index = {}
print("Let's get this show on the road!")

ls = [idx for idx in os.listdir(list_path) if idx.endswith('.list')]
status = read_progress(progress_file)
if status:
    print("We're still in progress. Where are we at?")
    index = ls.index(status.get('last_parsed_index'))
    ls = ls[ls.index(index):]

ls = ['running-times.list']
print("Indexing {0} properties...".format(len(ls)))
for filename in ls:
    full_path = os.path.join(list_path, filename)
    index_name = filename.replace('.list', '').replace('-', '_')
    counter = 0

    print("Indexing {0} ...".format(index_name))
    with open(full_path, 'r') as csv_list:
        reader = csv.reader(csv_list, dialect='excel-tab')
        for row in reader:
            if len(row) > 1:
                idx = row[0].decode('latin-1').encode("utf-8")
                val = ''.join(row[1:]).decode('latin-1').encode("utf-8")
                matches = re.search('(\(\d{4}\))', idx)
                if matches:
                    year = matches.group(0)[1:5]
                    movie_name = idx.replace("({0})".format(year), "").strip()
                else:
                    year = "?"
                    movie_name = idx

                splitted_val = val.split(':')
                if len(splitted_val) > 1:
                    origin = splitted_val[0]
                    running_time = splitted_val[1]
                else:
                    origin = 'Unknown'
                    running_time = val
                movie_dict = movie_index.get(movie_name, {})
                movie_dict.update({'year': year})
                movie_dict.update({'origin': origin})
                movie_dict.update({'running_time': running_time})
                movie_index.update({movie_name: movie_dict})
                counter += 1

                step = counter / 100000
                if step == int(step):
                    print("Indexed {0} from {1}....".format(counter, index_name))
    # Store progress to file, store content to file.
    finished = ls.index(filename) == len(ls)
    store_progress(index_name, counter, finished, progress_file)
    store_content(movie_index, results_file)

print("Finished! ")
