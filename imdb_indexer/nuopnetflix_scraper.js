// url: http://www.nuopnetflix.nl/aanbod-netflix

var content = jQuery('.releases li').not('class');
var movies = [];

for(i=0;i<content.length;i++){
    var el = jQuery(content[i]);
    var netflix_score = '';
    var genre = '';
    var year = '';
    
    var title = el.find('span.title a').html();
    var genre_str = el.find('.expanded .genre').html();
    if (genre_str){
    	var genre = genre_str.replace('Genre: ', '');
    }
    
    var rating = el.find('.expanded .imdb:last').html();
    if (rating && rating.indexOf('flix rating') > 0){
    	var netflix_score = rating.replace('Netflix rating: ','');
    }

    var year_cont = el.find('.year').html();
    if (year_cont){
        var year = year_cont.replace('Jaren: ', '');
    }

    var res = {
		title: title,
        genre: genre,
        netflix_score: netflix_score,
        year: year
    };
    movies.push(res);
}

// Store in clipboard
copy(movies);

// todo rewrite to bs4