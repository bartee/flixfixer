// pathethuis films
// met selenium klikken tot geen meer films tonen meer komt.

title_path = '.list-info h4';
rating = '.sideinfo .rating-stars span'.class;

var content = jQuery('a.poster');
var movies = [];
for (i=0; i<content.length; i++){
	var el = jQuery(content[i]);
	var url = el.attr('href');
	var rating = '';
	
	var title = el.find('.list-info h4').html();	
	var rating_list = el.find('.sideinfo .rating-stars span:first');
	if (rating_list){
		var rating = rating_list[0].classList[0].replace('rating-stars-', '');
	}
	var res = {
		title: title,
		url: url,
		rating: rating,
	}
	movies.push(res);
}

// copy to memory
copy(movies);