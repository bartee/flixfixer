FlixFixer
---------

Ever wondered, that you'd like to watch a movie, yet you do not know what mood you're in and just want to be surprised?
Ever been digging through Netflix to find either movies, or something that fits your need,
or wondering if you've seen it before?

And finding out afterwards that you've actually spent an hour doing just that and having no more time to watch it?

In come FlixFixer. You give it the amount of time you have to burn, maybe some preferences - maybe not, and hit "search"
You'll find a list of movies, ordered by your liking.

Set up
------
It's also an experiment:
- Django with Elastic Search
- Django using channels for autocomplete

Here's how the data's organised
-------------------------------

ES-movies:
- IMDb index, only with runningtime, Movie Name, and movie year. Might have some kind of region.

MySQL:

- StreamProviders - Netflix, Pathe, Amazon Prime probably, VideoLand. Generic objects
- StreamProviderMoviesLinks
An index for the given movie in the given streamprovider, a local rating.

Realtime Data Enrichment
------------------------
If there's no imdb data available, enrich the data using imdbpy. Finding imdbID, posterURL, imdb score, synopsis.
That new data has to be incorporated in ElasticSearch.

Obsolete Data
-------------
In case the data for a movie is wrong, an admin has to be able to trigger a re-index by flushing the ES-entry for
the movie, storing only the base info: movie name, running time and movie year. Maybe the region.

Plan
----
* ( ) Build models, for both ES movies as well as MySQL models
* ( ) Load the data into the databases
* ( ) Build the search and the display.
* ( ) Build enrichment routine
* ( ) Build flushing routine