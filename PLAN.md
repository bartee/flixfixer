take running-times.list leading
    - title
    - year
    - running-time
    - origin (optional)

search: running time-> list of titles

DATA ENRICHMENT
    http://imdbpy.sourceforge.net/
    - omdbapi:
        - lazy data enrichment:
        imdbID --> link
        type
        poster_url (optional)
    - imdb (scraping after visiting the link)
        score

NETFLIX/PATHE
    url
    streamservice_type
    last_found

streamservice
    name
    base_url

TITLE FLUSH
    - in case the info is wrong, flush it. 


MODELS
    movie
        title
        year
        running_time
        origin
        _last_indexed
        imdbID
        type
        poster_url
        trailer_url
