from django.core.management.base import BaseCommand, CommandError
from elasticsearch.helpers import bulk
from flixfixer.models import Movie
import os
import json
import sys
import logging
logger = logging.getLogger('populate_logger')


class Command(BaseCommand):
    """
    Create the ElasticSearch index and add the mappping.

    This command is for starting your index anew; there are *no* duplication checks!
    """
    can_import_settings = True
    help = "Import all indiced movie info. Movie name, movie year, running time and - if known - the origin country"

    def handle(self, *args, **options):
        from django.conf import settings
        # Read the file from settings
        filename = os.path.abspath(os.path.join(settings.PROJECT_ROOT, settings.MOVIE_INDEX_FILE))

        # sys.setdefaultencoding() does not exist, here!
        reload(sys)  # Reload does the trick!
        sys.setdefaultencoding('UTF8')

        if os.path.isfile(filename):
            content = json.load(open(filename))
            dummy_instance = Movie()
            self.metadata = {
                '_op_type': 'index',
                '_index': dummy_instance._meta.es_index_name,
                '_type': dummy_instance._meta.es_type_name,
            }
            data = []
            counter = 0;
            print "Creating objects!"
            for movie_name, movie_context in content.items():
                movie = self.create_es_context(movie_name, movie_context)
                if movie:
                    # movie.update({'_id': counter})
                    data.append(movie)
                    counter += 1
                    step = counter / 100
                    if step == int(step):
                        # print('Created {0} objects before converting'.format(counter))
                        bulk(client=settings.ES_CLIENT, actions=data, stats_only=True)
                        data = []

            # Here's the real magic!
            print('Created {0} objects. Inserting!'.format(counter))
            bulk(client=settings.ES_CLIENT, actions=data, stats_only=True)
            print('Database has been updated.')
        else:
            basename = os.path.basename(filename)
            print('File {0} not found!'.format(basename))


    def create_es_context(self, name, context):
        """
        Create an object, and convert the context into a dict

        :param name:
        :param context:
        :return:
        """
        data = {}
        data['name'] = name
        data['origin'] = context.get('origin')

        try:
            data['running_time'] = int(context.get('running_time'))
        except ValueError:
            logger.warning("%s does not have valid runtime info in dict: %s",
                           name.decode('latin1'),
                           context.get('running_time'))
            return None
        try:
            data['year'] = int(context.get('year'))
        except ValueError:
            logger.warning("%s does not have valid year info in dict: %s",
                           name.decode('latin1'),
                           context.get('year'))
            return None
        # data['_id'] = md5('{0} ({1})'.format(name, data['year'])).hexdigest()
        data.update(**self.metadata)

        return data
