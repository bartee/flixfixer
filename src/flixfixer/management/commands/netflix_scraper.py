from django.core.management.base import BaseCommand, CommandError
from lxml import html
import requests
from flixfixer.models import StreamService
from flixfixer.services import MovieSearchService
import logging

logger = logging.getLogger(__file__)


class Command(BaseCommand):

    url = 'http://www.nuopnetflix.nl/aanbod-netflix'
    # http://www.pathe-thuis.nl/movie/index/browse?mainURL=&subURL=&page=2&amount=5&sort=name%2Basc
    xpath_elements = '//ul[@class="releases"]/li'

    def __init__(self):
        super(Command, self).__init__()
        self.netflix_obj = StreamService.objects.get_or_create(name='Netflix', base_url='https://www.netflix.com')
        self.mss = MovieSearchService('django', 'movies')

    def handle(self, *args, **options):
        """
        Scrape all entries from nuopnetflix. There might be a better reference, but this works for now.

        After scraping, find the movie source from elasticsearch by finding the movie name and year.
        Store it in the MySQL data

        :param args:
        :param options:
        :return:
        """
        all_entries = []
        errors = []
        page = requests.get(self.url)
        tree = html.fromstring(page.content)
        single_entries = tree.xpath(self.xpath_elements)

        # Gather all available Netflix entries from www.nuopnetflix.nl
        for entry in single_entries:
            genre = ''
            netflix_score = ''
            year = 'unknown'
            try:
                title_el = entry.xpath('span[@class="title"]/a')[0]
                genre_el = entry.xpath('.//span[@class="genre"]')[0]
                year_el = entry.xpath('.//span[@class="year"]')[0]
                rating_el = entry.xpath('.//span[@class="imdb"]').pop()

                title = title_el.text_content()
                if isinstance(genre_el, html.HtmlElement):
                    genre = genre_el.text_content().replace('Genre: ','')
                if isinstance(rating_el, html.HtmlElement) and rating_el.text.find('flix rating') > 0:
                    netflix_score = rating_el.text_content().replace('Netflix rating: ', '')

                if isinstance(year_el, html.HtmlElement):
                    year_text = year_el.text_content()
                    # if year_text.match('Jaren'):
                    #     # It's a serie. Don't enrich it yet.
                    #     series = True
                    years = year_text.split(': ').pop()
                    year = years

                    # Years might be a list. Only take the first one in that case.
                    if years.find('-') > 0:
                        # Only take the first one
                        year = years.split('-')[0]


                res = {
                    'title': title,
                    'genre': genre,
                    'rating': netflix_score,
                    'year': year
                }

                # Find the movie in the elasticsearch index
                imdb_data = self.search_for_movie(title, year)
                if imdb_data:
                    res.update({'id': imdb_data.get('id')})

                all_entries.append(res)
            except IndexError, e:
                print "Unknown element: \n ----- \n"
                print entry.text_content()
                print e.message
                errors.append(e)
                continue
        print "Finished scraping! Here's the result: "
        print all_entries


    def search_for_movie(self, title, year):
        """
        Search for the movie in the index. Return the first item, if it matches exactly.

        :param title:
        :param year:
        :return:
        """
        year_query = {'match': {'year': year}}
        name_query = {'match': {'name': title}}

        query = {'bool': {'must': [year_query, name_query]}}

        result = self.mss.search(query_dict=query)
        # Validate result[0]
        data_to_verify = result[0]
        if data_to_verify.get('name') == title and data_to_verify.get('year') == year:
            return result[0]

        # Else,
        logger.warning('Movie not found in index: {0} ({1})'.format(title, year))
        return None