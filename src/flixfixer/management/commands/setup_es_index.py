from django.core.management.base import BaseCommand
from elasticsearch.client import IndicesClient
from flixfixer.models import Movie


class Command(BaseCommand):
    """
    Create the ElasticSearch index and add the mappping
    """
    can_import_settings = True

    def handle(self, *args, **options):
        from django.conf import settings
        indices_client = IndicesClient(client=settings.ES_CLIENT)
        index_name = Movie._meta.es_index_name

        if indices_client.exists(index_name):
            indices_client.delete(index=index_name)

        indices_client.create(index=index_name)
        indices_client.put_mapping(
            doc_type=Movie._meta.es_type_name,
            body=Movie._meta.es_mapping,
            index=index_name
        )
