from django.apps import AppConfig


class FlixfixerConfig(AppConfig):
    name = 'flixfixer'
