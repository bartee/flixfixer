from django.contrib import admin
from flixfixer.models import StreamService, StreamServiceMovie

admin.site.register(StreamService)
admin.site.register(StreamServiceMovie)
