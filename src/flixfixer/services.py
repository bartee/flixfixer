from django.conf import settings
client = settings.ES_CLIENT


class MovieSearchService(object):

    def __init__(self, index, doc_type):
        """

        :param index:
        :param doc_type:
        """
        self.index = index
        self.doc_type = doc_type

    def url(self):
        return '{0}/{1}/{2}'.format(settings.ES_CONFIG, self.index, self.doc_type)

    def search(self, query_dict, plain=True):
        """
        Search for a certain thingie

        :param query_dict:
        :return:
        """
        body = {}
        body.update({'query': query_dict})
        search_result = client.search(index=self.index, doc_type=self.doc_type, body=body)
        hits = []
        for raw_hit in search_result['hits']['hits']:
            print raw_hit
            hit = {
                'id': raw_hit['_id'],
                'data': raw_hit['_source']
            }
            hits.append(hit)
        return hits

    def search_by_name(self, name):
        query = {
            'name': name
        }