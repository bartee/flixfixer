# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2017-01-17 14:12
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('flixfixer', '0002_movie'),
    ]

    operations = [
        migrations.RenameField(
            model_name='streamservicemovie',
            old_name='movie_name',
            new_name='movie_index',
        ),
    ]
