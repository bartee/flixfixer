from django.db import models
import django.db.models.options as options
options.DEFAULT_NAMES = options.DEFAULT_NAMES + (
    'es_index_name', 'es_type_name', 'es_mapping'
)


class StreamService(models.Model):
    """
    The StreamService
    """
    name = models.CharField(max_length=100)
    base_url = models.URLField()

    def __str__(self):
        return self.name


class StreamServiceMovie(models.Model):
    """

    """
    movie_index = models.CharField(max_length=255)
    service = models.ForeignKey(StreamService)
    rating = models.IntegerField(max_length=10)
    data = models.TextField(blank=True)

    def __str__(self):
        return "{0} ({1}".format(self.movie_name, self.service.name)


class Movie(models.Model):

    class Meta:
        es_index_name = 'django'
        es_type_name = 'movies'
        es_mapping = {
            'properties': {
                'name': {
                    'type': 'text'
                },
                'running_time': {
                    'type': 'integer'
                },
                'year': {
                    'type': 'short'
                },
                'origin': {
                    'type': 'text'
                },
                'type': {
                    'type': 'keyword'
                },
                'genres': {
                    'type': 'keyword'
                },
                'poster': {
                    'type': 'text'
                },
                'rating': {
                    'type': 'short'
                }
            }
        }